__author__ = 'Maciej'

# biblioteka do odczytywania plikow *.xlsx
from openpyxl import load_workbook
# biblioteka do tworzenia interfejsu graficznego
from Tkinter import *
# modul wczytywania pliku do programu
from tkFileDialog import askopenfilename
# widzet do wyswietlania wykresu na ekranie
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
# biblioteka do rysowania wykresow
from matplotlib.figure import Figure
# biblioteka matematyczna (wszelkie operacje macierzowe)
import numpy as np


class Application(Frame):
    def openfile(self):
        # wybor pliku
        filename = askopenfilename()
        # zaladowanie danych
        self.step = self.radioValue.get()
        dates, data = load_data(filename, self.optionValue.get(), self.step)
        self.data = data
        # wyliczanie najlepszych parametrow dla Holt'a
        a, b = holt_params(self.data, self.step)
        self.a.delete(0, 'end')
        self.b.delete(0, 'end')
        self.a.insert(0, a)
        self.b.insert(0, b)
        # czyszczenie list
        self.clear_lists()
        # czyszczenie wykresow
        self.clear_plots()
        # wpisanie danych do list
        j = 1
        for i in range(len(data)):
            if j > self.step:
                j = 1
            self.data_listbox.insert(END, data[i])
            self.dates_listbox.insert(END, dates[i])
            self.step_listbox.insert(END, j)
            j += 1

    # metoda czyszczaca wszystkie listy
    def clear_lists(self):
        self.data_listbox.delete(0, END)
        self.dates_listbox.delete(0, END)
        self.step_listbox.delete(0, END)
        self.holt_result_listbox.delete(0, END)
        self.mnk_result_listbox.delete(0, END)

    # metoda czyszczaca wszystkie wykresy
    def clear_plots(self):
        self.mnk_plot.clear()
        self.holt_plot.clear()
        self.canvas1.draw()
        self.canvas2.draw()

    # metoda rysujaca wykresy
    def draw_plot(self, trend_mnk, g_mnk, holt_y):
        # czyszczenie wykresow
        self.clear_plots()

        # naniesienie wynikow MNK
        self.mnk_plot.set_ylim(min(self.data) - 1, max(self.data) + 1)
        self.mnk_plot.plot(self.data)
        self.mnk_plot.plot(trend_mnk, 'r')
        self.mnk_plot.plot([x + len(self.data) for x in range(self.step)], g_mnk, 'g+')

        # naniesienie wynikow Holta
        self.holt_plot.set_ylim(min(self.data + holt_y) - 1, max(self.data + holt_y) + 1)
        self.holt_plot.plot(self.data)
        self.holt_plot.plot(range(1, len(holt_y)), holt_y[1:], 'g')
        self.holt_plot.plot(range(1, len(holt_y)), holt_y[1:], 'g+')

        # narysowanie wykresow
        self.canvas1.draw()
        self.canvas2.draw()

    def calculate(self):
        # obliczanie wartosci przewidywanych
        trend_mnk, g_mnk = least_squares(self.data, self.step, int(self.step_listbox.get(END)) + 1)
        holt_y, mse = holt(self.data, self.step, self.a.get(), self.b.get())
        # wyswietlenie bledow MSE oraz RMSE
        self.mse_var.set(str(round(mse, 4)))
        self.rmse_var.set(str(round(np.sqrt(mse), 4)))
        # wyczyszczenie list z wynikami
        self.mnk_result_listbox.delete(0, END)
        self.holt_result_listbox.delete(0, END)
        # wprowadzenie wynikow do list
        for elem in g_mnk:
            self.mnk_result_listbox.insert(END, round(elem, 2))

        for elem in holt_y[len(self.data):]:
            self.holt_result_listbox.insert(END, round(elem, 2))

        # rysowanie wynikow
        self.draw_plot(trend_mnk, g_mnk, holt_y)

    def createWidgets(self):

        # ramka na przyciski do obslugi programu
        self.frame = Frame(self)
        self.frame.pack(anchor=SW, side="bottom", fill=BOTH, expand=1)

        # przycisk zamkniecia
        self.QUIT = Button(self.frame)
        self.QUIT["text"] = "QUIT"
        self.QUIT["fg"] = "red"
        self.QUIT["command"] = self.quit
        self.QUIT.pack({"anchor": SE, "side": "right", "padx": 10, "pady": 10})

        # pzycisk zaladowania pliku
        self.open = Button(self.frame)
        self.open["text"] = "Open file"
        self.open["command"] = self.openfile
        self.open.pack({"anchor": SE, "side": "right", "padx": 10, "pady": 10})

        # przycisk uruchamiajacy obliczenia
        self.run = Button(self.frame)
        self.run["text"] = "Calculate"
        # self.run["command"] = self.draw_plot
        self.run["command"] = self.calculate
        self.run.pack({"anchor": SE, "side": "right", "padx": 10, "pady": 10})

        # wybor kroku dla szeregu czasowego
        self.MODES = [
            ("Miesieczne", 12),
            ("Kwartalne", 4),
            ("Roczne", 1),
        ]

        self.radioValue = IntVar()
        self.radioValue.set(12)

        for text, mode in self.MODES:
            self.radio = Radiobutton(self.frame, text=text,
                                     variable=self.radioValue, value=mode)
            self.radio.pack(anchor=SE, side="bottom", padx=10)

        # wybor ilosci lat
        OPTIONS = range(1, 11)

        self.optionValue = IntVar(self)
        self.optionValue.set(OPTIONS[4])  # default value

        self.optionMenu = apply(OptionMenu, (self.frame, self.optionValue) + tuple(OPTIONS))
        self.optionMenu.pack(anchor=SE, side="right", pady=10, padx=10)

        self.year_label = Label(self.frame, text="Ilosc lat:")
        self.year_label.pack(anchor=E, side="right", padx=1)

        # etykieta wspolczynnika a
        self.a_label = Label(self.frame, text="Wspolczynnik a:")
        self.a_label.pack(anchor=E, side="left", padx=1)
        # wejscie wspolczynnika a
        self.a = Entry(self.frame)
        self.a.pack(anchor=E, side="left", padx=10, fill=None, expand=False)
        # etykieta wspolczynnika b
        self.b_label = Label(self.frame, text="Wspolczynnik b:")
        self.b_label.pack(anchor=E, side="left", padx=1)
        # wejscie wspolczynnika b
        self.b = Entry(self.frame)
        self.b.pack(anchor=E, side="left", padx=10, fill=None, expand=False)

        # etykieta MSE
        self.mse_label = Label(self.frame, text="MSE:")
        self.mse_label.pack(anchor=E, side="left", padx=1)
        # blad MSE
        self.mse_var = StringVar()
        self.mse = Label(self.frame, textvariable=self.mse_var)
        self.mse.pack(anchor=E, side="left", padx=1)
        # etykieta RMSE
        self.rmse_label = Label(self.frame, text="RMSE:")
        self.rmse_label.pack(anchor=E, side="left", padx=1)
        # blad RMSE
        self.rmse_var = StringVar()
        self.rmse = Label(self.frame, textvariable=self.rmse_var)
        self.rmse.pack(anchor=E, side="left", padx=1)

        # scrollbar dla obu list
        self.scroll = Scrollbar(self, orient=VERTICAL)
        self.scroll.pack(side=RIGHT, fill=Y)

        # lista dat
        self.data_listbox = Listbox(self, yscrollcommand=self.scroll.set)
        self.data_listbox.pack({"anchor": NE, "side": "right", "fill": 'y', "padx": 10, "pady": 10})

        # lista okresow
        self.step_listbox = Listbox(self, yscrollcommand=self.scroll.set)
        self.step_listbox.pack({"anchor": NE, "side": "right", "fill": 'y', "ipadx": 10, "pady": 10})

        # lista danych
        self.dates_listbox = Listbox(self, yscrollcommand=self.scroll.set)
        self.dates_listbox.pack({"anchor": NE, "side": "right", "fill": 'y', "padx": 10, "pady": 10})
        self.scroll.configure(command=self.yview)

        # frame z wynikami
        self.result_frame = Frame(self)
        self.result_frame.pack(anchor=NE, side="right", fill="y", padx=10, pady=10)

        # etykieta dla wynikow mnk
        self.mnk_result_label = Label(self.result_frame, text="Wyniki MNK")
        self.mnk_result_label.pack(side="top", pady=10)

        # Lista z wynikami MNK
        self.mnk_result_listbox = Listbox(self.result_frame)
        self.mnk_result_listbox.pack(side="top", pady=10, fill="both", expand=1)

        # etykieta dla wynikow Holta
        self.holt_result_label = Label(self.result_frame, text="Wyniki Holt")
        self.holt_result_label.pack(side="top", pady=10)

        # Lista z wynikami MNK
        self.holt_result_listbox = Listbox(self.result_frame)
        self.holt_result_listbox.pack(side="top", pady=10, fill="both", expand=1)

        # wykres MNK
        self.fig1 = Figure(figsize=(8, 3), dpi=100)
        self.mnk_plot = self.fig1.add_subplot(111)
        self.mnk_plot.plot([0], [0])

        self.canvas1 = FigureCanvasTkAgg(self.fig1, master=self)
        self.canvas1.show()
        self.canvas1.get_tk_widget().pack(side='top', fill='both', expand=1)

        # wykres Holta
        self.fig2 = Figure(figsize=(8, 3), dpi=100)
        self.holt_plot = self.fig2.add_subplot(111)
        self.holt_plot.plot([0], [0])

        self.canvas2 = FigureCanvasTkAgg(self.fig2, master=self)
        self.canvas2.show()
        self.canvas2.get_tk_widget().pack(anchor=SW, side='top', fill='both', expand=1)

    # metoda przypisujaca 3 listy do jednego scrollbara
    def yview(self, *args):
        apply(self.data_listbox.yview, args)
        apply(self.dates_listbox.yview, args)
        apply(self.step_listbox.yview, args)

    # konstruktor okna
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.pack()
        self.createWidgets()


def load_data(filename, years, step):
    # zaladowanie pliku
    wb = load_workbook(filename, read_only=True)

    # wybor arkusza
    ws = wb.active

    # inicjacja pustych list na dane
    dates = []
    data = []
    amount = years * step
    end = "B" + str(amount)

    # zaladowanie danych
    cell_range = ws['A1':end]
    for row in cell_range:
        dates.append(row[0].value)
        data.append(row[1].value)

    return dates, data


def least_squares(data, step, begin):
    # tworzenie macierzy
    xtemp = []
    for i in range(len(data)):
        xtemp.append(1)
        xtemp.append(i + 1)

    x = np.matrix(xtemp).reshape(len(xtemp) / 2, 2)
    y = np.matrix(data).reshape(len(data), 1)

    # wyliczenie wspolczynnikow a i b
    result = np.linalg.inv((x.getT() * x)) * (x.getT() * y)
    a = result[0, 0]
    b = result[1, 0]

    # wyliczanie trendu
    trend_mnk = [b * i + a for i in range(len(data) + step)]

    # tworzenie listy okresow
    j = 1
    intervals = []
    for i in range(len(data)):
        if j > step:
            j = 1
        intervals.append(j)
        j += 1

    # wyliczanie wspolczynnikow S
    tempy = [[] for i in range(step)]
    temp_trend_mnk = [[] for i in range(step)]
    for i in range(len(data)):
        tempy[intervals[i] - 1].append(data[i])
        temp_trend_mnk[intervals[i] - 1].append(trend_mnk[i])

    s = []
    for i in range(step):
        s.append(np.mean(tempy[i]) - np.mean(temp_trend_mnk[i]))

    # oliczanie przewidywanych wartosci
    g = []
    begin = range(begin, step + 1) + range(0, begin)
    for i in range(step):
        g.append(trend_mnk[len(data) + i - 1] + s[begin[i]])

    return trend_mnk, g


def holt(data, step, a, b):
    # rzutowanie wspolczynnikow a i b na wartosci zmiennoprzecinkowe
    a = float(a)
    b = float(b)
    # inicjalizacja tablic
    f = []
    s = []
    y = []
    f.append(data[0])
    y.append(data[0])
    s.append(data[1] - data[0])

    # obliczanie wspolczynnikow f, s oraz y
    for t in range(1, len(data)):
        f.append(a * data[t] + (1 - a) * (f[t - 1] + s[t - 1]))
        s.append(b * (f[t] - f[t - 1]) + (1 - b) * s[t - 1])
        y.append(f[t - 1] + s[t - 1])

    # obliczanie wartosci prognozowanych y
    for t in range(len(data) - 1, len(data) + step - 1):
        y.append(f[-1] + (t + 1 - len(data) + 1) * s[-1])

    # wyliczanie bledu MSE
    mse_temp = []
    for t in range(1, len(data)):
        mse_temp.append(pow(data[t] - y[t], 2))

    return y, np.mean(mse_temp)


def holt_params(data, step):
    # inicjalizacja zmiennej przechowujacej najmniejszy blad MSE
    best_mse = None
    for a in np.arange(0.01, 1.01, 0.01):
        for b in np.arange(0.01, 1.01, 0.01):
            # inicjalizacja tablic
            f = []
            s = []
            y = []
            f.append(data[0])
            y.append(data[0])
            s.append(data[1] - data[0])
            # obliczanie wspolczynnikow f, s oraz y
            for t in range(1, len(data)):
                f.append(a * data[t] + (1 - a) * (f[t - 1] + s[t - 1]))
                s.append(b * (f[t] - f[t - 1]) + (1 - b) * s[t - 1])
                y.append(f[t - 1] + s[t - 1])

            # obliczanie wartosci prognozowanych y
            for t in range(len(data) - 1, len(data) + step):
                y.append(f[-1] + (t + 1 - len(data) + 1) * s[-1])

            # obliczanie bledu MSE
            mse_temp = []
            for t in range(1, len(data)):
                mse_temp.append(pow(data[t] - y[t], 2))
            mse = np.mean(mse_temp)

            # jesli aktualny MSE mniejszy, zapisz a i b
            if (best_mse is None) or (mse < best_mse):
                best_a = round(a, 2)
                best_b = round(b, 2)
                best_mse = mse

    return best_a, best_b

# funkcja uruchamiajaca program
def main():
    root = Tk()
    app = Application(master=root)
    root.title('Prognozowanie')
    app.mainloop()
    root.destroy()


if __name__ == '__main__':
    main()
